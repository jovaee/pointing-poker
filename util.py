import os
import sqlite3

from flask import g

APPDATA_DIR = os.environ.get("APPDATA")
if not APPDATA_DIR:
    print("No APPDATA directory specified, default to root")
    APPDATA_DIR = "."

DATABASE = os.path.join(APPDATA_DIR, "database.db")


def make_dicts(cursor, row):
    return {cursor.description[idx][0]: value for idx, value in enumerate(row)}


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE, isolation_level=None)
        db.row_factory = make_dicts

    return db


def get_names() -> list[str]:
    names = getattr(g, "_names", None)
    if names is None:
        with open("data/names.txt") as names_file:
            names = g._names = [name.strip() for name in names_file]

    return names
