import os

from flask import Flask, g
from flask_cors import CORS
from flask_socketio import SocketIO

import routes.api as api_routes
import routes.sockets as socket_routes

app = Flask(__name__, static_folder="view/dist", static_url_path="")
socketio = SocketIO(app, cors_allowed_origins="*", logger=True)
CORS(app)


# Match the /room path for which it will return the index.html file if the requested subpath resource
# does not exist. It will correctly return js resource etc from static folder.
# TODO: Find some better way to do this. Manually have to add client side paths so that other status
# resource will keep being returned correctly on page refresh for that view url
@app.route('/', defaults={'path': ''})
@app.route("/session/<path:path>")
def serve(*_, **__):
    return app.send_static_file("index.html")


# Close DB connection at end of request
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()


# Setup API routes
app.add_url_rule('/api/user/', view_func=api_routes.get_users, methods=["GET"])
app.add_url_rule('/api/user/', view_func=api_routes.create_user, methods=["POST"])
app.add_url_rule('/api/session/', view_func=api_routes.create_session, methods=["POST"])
app.add_url_rule('/api/session/<string:session_id>', view_func=api_routes.get_session, methods=["GET"])
app.add_url_rule('/api/generate/name', view_func=api_routes.get_random_name, methods=["GET"])
app.add_url_rule('/api/generate/gif', view_func=api_routes.get_random_gif_by_phrase, methods=["GET"])


# Setup socketio event routes
socketio.on_event('connect', socket_routes.connect)
socketio.on_event('disconnect', socket_routes.disconnect)
socketio.on_event('join', socket_routes.join)
socketio.on_event('sessionUpdate', socket_routes.session_update)
socketio.on_event('vote', socket_routes.vote)
socketio.on_event('reset', socket_routes.reset)
socketio.on_event('flip', socket_routes.flip)
socketio.on_event('userMoveUpdate', socket_routes.user_move_update)
socketio.on_event('speak', socket_routes.speak)


if __name__ == "__main__":
    port = os.getenv('PORT', 80)
    socketio.run(app, port=int(port), debug=os.environ.get('DEBUG', "false").lower() == "true")
    