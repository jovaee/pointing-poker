#!/bin/bash

export APPDATA="appdata"
export DEBUG="true"

gunicorn --reload -k eventlet -w 1 server:app