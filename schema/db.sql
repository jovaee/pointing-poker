CREATE TABLE user (
	user_id 				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	session_id   			TEXT NOT NULL,
	username     			TEXT,
	background_image_url 	TEXT,
	score	     			TEXT,
	old_score 	 			TEXT,
	vote_flagged 			TINYINT(1) NOT NULL DEFAULT 0,
	disconnected			TINYINT(1) NOT NULL DEFAULT 0,
	is_spectator			TINYINT(1) NOT NULL,
	sid	         			TEXT UNIQUE
);

CREATE TABLE session (
	session_id	  TEXT NOT NULL PRIMARY KEY UNIQUE,
	description	  TEXT,
	flipped       TINYINT DEFAULT 0,
	date_created  NUMERIC NOT NULL
);