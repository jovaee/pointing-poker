import os
import requests
import random

import nanoid

from datetime import datetime
from typing import Dict, List

from flask import abort, request

from util import get_db, get_names


KEY = os.environ.get('GIPHY_API_KEY')
API = "https://api.giphy.com/v1/gifs/random?api_key={key}&tag={phrase}&rating=r"


# User
def get_users() -> dict[str, list[dict]]:
    """
    Get all users
    """
    cursor = get_db().cursor()
    users = cursor.execute("SELECT * FROM user", []).fetchall()
    return {"users": users}


def create_user() -> dict:
    """
    Create a new user in a pointing poker session
    """
    if not request.json or any([a not in request.json for a in ["username", "session_id"]]):
        abort(400)

    cursor = get_db().cursor()

    # Make sure the session exists
    session = cursor.execute("SELECT session_id FROM session WHERE session_id = ?", [request.json["session_id"]]).fetchone()
    if not session:
        abort(404)

    # Insert the user
    cursor.execute(
        "INSERT INTO user (username, background_image_url, session_id, is_spectator) VALUES (?, ?, ?, ?)",
        [
            request.json["username"],
            request.json.get("backgroundImageUrl"),
            session["session_id"],
            request.json.get("is_spectator", False)
        ]
    )
    return cursor.execute("SELECT * FROM user WHERE user_id = ?", [cursor.lastrowid]).fetchone()


# Session
def get_session(session_id: str) -> dict:
    """
    Get a specific pointing poker session
    """
    cursor = get_db().cursor()
    session = cursor.execute("SELECT * FROM session WHERE session_id = ?", [session_id]).fetchone()
    if not session:
        abort(404)
    else:
        return session


def create_session() -> dict:
    """
    Create a new pointing poker session
    """
    session_id = nanoid.generate(size=10)

    cursor = get_db().cursor()
    cursor.execute("INSERT INTO session (session_id, date_created) VALUES (?, ?)", [session_id, int(datetime.now().timestamp())])
    return cursor.execute("SELECT * FROM session WHERE session_id = ?", [session_id]).fetchone()


# Generate
def get_random_name() -> dict:
    names = get_names()

    choices = random.choices(names, k=2)
    return {"name": " ".join(choices)}


def get_random_gif_by_phrase() -> dict:
    phrase = request.args.get('phrase')
    if not phrase:
        abort(400)
    
    response = requests.get(API.format(key=KEY, phrase=phrase)).json()
    url = response["data"]["images"]["original"]["url"]

    return {"url": url}
