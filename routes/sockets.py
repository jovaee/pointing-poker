from collections import Counter
from collections.abc import Callable
from typing import Any

from flask import request
from flask_socketio import close_room, emit, join_room

from util import get_db


def isfloat(num: Any) -> bool:
    try:
        float(num)
    except (TypeError, ValueError):
        return False
    else:
        return True


def closest_fibonacci(number: int | float) -> int:
    if number < 0:
        return 1

    # Compute Fibonacci numbers
    # This can technically be cached but very small input numbers so not that worried about this for now
    fib_numbers = [0, 1]
    while fib_numbers[-1] < number:
        next_fib = fib_numbers[-1] + fib_numbers[-2]
        fib_numbers.append(next_fib)

    # Check the closest Fibonacci numbers
    closest_lower = fib_numbers[-2] if fib_numbers[-1] > number else fib_numbers[-1]
    closest_higher = fib_numbers[-1]

    # Determine which one is closer
    return closest_lower if abs(number - closest_lower) <= abs(number - closest_higher) else closest_higher



def calculate_stats(sid: str | None = None, session_id: str | None = None):
    cursor = get_db().cursor()
    
    if not sid and not session_id:
        print("No ID provided so cannot recalculate stats")
        return

    if sid:
        sql = "SELECT * FROM user JOIN (SELECT session_id FROM user WHERE sid = ?) USING (session_id) WHERE disconnected = 0 AND is_spectator = 0"
        params = [sid]
    else:
        sql = "SELECT * FROM user WHERE session_id = ?"
        params = [session_id]

    users = cursor.execute(sql, params).fetchall()

    # Calculate the average points voted
    if users:
        avg = 0
        closest_fib = 0
        most_common = 0
        min_ = 0
        max_ = 0
        try:
            scores = [float(u["score"]) for u in users if isfloat(u["score"])]
            avg = sum(scores) / len(scores)
            closest_fib = closest_fibonacci(avg)
            most_common = max(scores, key=Counter(scores).get)
            min_ = min(scores)
            max_ = max(scores)
        except (ValueError, ZeroDivisionError):
            pass
        finally:
            def get_format(num):
                return "{:.0f}" if num % 1 == 0 else "{:.2f}"
           
            emit(
                'sessionStatsUpdated',
                {
                    'min': get_format(min_).format(min_),
                    'max': get_format(max_).format(max_),
                    'average': get_format(avg).format(avg),
                    'closest_fib': get_format(closest_fib).format(closest_fib),
                    'most_common': get_format(most_common).format(most_common),
                },
                to=users[0]["session_id"]
            )


def stats_calculate_decorator(f: Callable) -> Callable:

    def inner(*args, **kwargs):
        f(*args, **kwargs)
        calculate_stats(sid=request.sid)

    return inner


def publish_notification(session_id: int, title: str, description: str | None, status: str):
    emit('notification', {'title': title, 'description': description, 'status': status}, to=session_id)


def connect():
    print(f'Client connected {request.sid}')


def flip():
    """
    Someone pressed the flip button
    """
    cursor = get_db().cursor()
    
    user = cursor.execute("SELECT * FROM user WHERE sid = ?", [request.sid]).fetchone()
    session_id = user["session_id"]

    session = cursor.execute("SELECT * FROM session WHERE session_id = ?", [session_id]).fetchone()
    
    # Only update and emit events when the session isn't already flipped
    if not session["flipped"]:
        cursor.execute("UPDATE session SET flipped = ? WHERE session_id = ?", [True, session_id])
        session = cursor.execute("SELECT * FROM session WHERE session_id = ?", [session_id]).fetchone()
        

        # Send all clients in room up to date data
        emit('sessionUpdated', {'session': session}, to=session_id)
        publish_notification(session_id, "Cards flipped", None, "success")


def disconnect():
    print(f'Client disconnected {request.sid}')

    cursor = get_db().cursor()

    # Delete the user and notify all other members in the room that they have left
    user = cursor.execute("SELECT * FROM user WHERE sid = ?", [request.sid]).fetchone()
    if user and user["session_id"]:
        session_id = user["session_id"]
        cursor.execute("UPDATE user SET disconnected = 1 WHERE user_id = ?", [user["user_id"]])
        users = cursor.execute("SELECT * FROM user WHERE session_id = ? AND disconnected = 0", [session_id]).fetchall()

        if len(users) == 0:
            close_room(session_id)
        else:
            publish_notification(session_id, "User left", f"{user['username']} left", "warning")
            calculate_stats(session_id=session_id)
            emit('usersChanged', {'users': users}, to=session_id, include_self=False)
            if all([u["score"] is not None or u["is_spectator"] for u in users]):
                # All users have voted show force a card flip
                flip()


@stats_calculate_decorator
def join(data):
    """
    Request right after user confirmed a connection. Initial data fetch
    """
    session_id = data["session_id"]
    user_id = data["user_id"]
    cursor = get_db().cursor()

    cursor.execute("UPDATE user SET sid = ?, disconnected = 0 WHERE user_id = ?", [request.sid, user_id])
    
    user = cursor.execute("SELECT * FROM user WHERE user_id = ?", [user_id]).fetchone()
    if not user:
        print(f"User {request.sid} join but not in the database")
        return
    
    users = cursor.execute("SELECT * FROM user WHERE session_id = ? AND disconnected = 0", [session_id]).fetchall()
    session = cursor.execute("SELECT * FROM session WHERE session_id = ?", [session_id]).fetchone()

    join_room(session_id)
    # Send all clients in room up to date data
    emit('usersChanged', {'users': users}, to=session_id)
    # Give latest session data to joined user
    emit('sessionUpdated', {'session': session})
    publish_notification(session_id, "User joined", f"{user['username']} joined", "success")


def session_update(data):
    """
    Session was updated somehow. Notify all other users
    
    TODO: Support more update fields
    """
    description = data["description"]

    cursor = get_db().cursor()

    user = cursor.execute("SELECT * FROM user WHERE sid = ?", [request.sid]).fetchone()
    session_id = user["session_id"]

    cursor.execute("UPDATE session SET description = ? WHERE session_id = ?", [description, session_id])
    session = cursor.execute("SELECT * FROM session WHERE session_id = ?", [session_id]).fetchone()

    # Send all clients in room up to date data
    emit('sessionUpdated', {'session': session}, to=session_id, include_self=True)


@stats_calculate_decorator
def vote(data):
    """
    A user changed their score
    """
    score = data["score"]

    cursor = get_db().cursor()
    user = cursor.execute("SELECT * FROM user WHERE sid = ?", [request.sid]).fetchone()
    session = cursor.execute("SELECT * FROM session WHERE session_id = ?", [user["session_id"]]).fetchone()
    
    # Keep the old score in sync with the current score as long as the cards aren't flipped
    old_score = user["old_score"]
    if not session["flipped"]:
        old_score = score

    # If a user voted again after the cards were flipped flag them, but only if the scores differ
    vote_flagged = False
    if session["flipped"] and old_score != str(score):
        vote_flagged = True

    cursor.execute("UPDATE user SET score = ?, old_score = ?, vote_flagged = ? WHERE sid = ?", [score, old_score, vote_flagged, request.sid])
    users = cursor.execute("SELECT * FROM user WHERE session_id = ? AND disconnected = 0", [user["session_id"]]).fetchall()

    # Send all clients in room up to date data
    emit('usersChanged', {'users': users}, to=user["session_id"])
    if all([u["score"] is not None or u["is_spectator"] for u in users]):
        # All users have voted show force a card flip
        flip()


@stats_calculate_decorator
def reset():
    """
    Session reset
    """
    cursor = get_db().cursor()

    user = cursor.execute("SELECT * FROM user WHERE sid = ?", [request.sid]).fetchone()
    session_id = user["session_id"]

    cursor.execute("UPDATE user SET score = ?, old_score = ?, vote_flagged = ? WHERE session_id = ?", [None, None, 0, session_id])
    cursor.execute("UPDATE session SET description = ?, flipped = ? WHERE session_id = ?", [None, False, session_id])
    users = cursor.execute("SELECT * FROM user WHERE session_id = ? AND disconnected = 0 AND is_spectator = 0", [session_id]).fetchall()
    session = cursor.execute("SELECT * FROM session WHERE session_id = ?", [session_id]).fetchone()

    # Send all clients in room up to date data
    emit('sessionUpdated', {'session': session}, to=session_id, include_self=True)
    emit('usersChanged', {'users': users}, to=session_id, includeSelf=True)
    publish_notification(session_id, "Room reset", None, "success")


def user_move_update(data):
    """
    User moved their mouse. Send the new location to all other clients.
    """
    # Send all clients in room the room the new location
    emit('userMoveUpdate', {'user_id': data["user_id"], 'posX': data["posX"], 'posY': data["posY"]}, to=data["session_id"], include_self=False)


def speak(data):
    """
    """
    generators = {
        "Average": lambda val: f"The average is {val}",
        "Minimum": lambda val: f"The minimum is {val}",
        "Maximum": lambda val: f"The maximum is {val}",
        "MostCommon": lambda val: f"The most common is {val}",
        "ClosestFib": lambda val: f"The closet fibonacci is {val}",
    }

    phrase_generator = generators.get(data["name"])
    if not phrase_generator:
        print(f"Unknown speak command {data['name']}")
        return
    
    cursor = get_db().cursor()
    user = cursor.execute("SELECT * FROM user WHERE sid = ?", [request.sid]).fetchone()
    session_id = user["session_id"]

    phrase_value = phrase_generator(data["value"])
    emit('say', {'phrase': phrase_value}, to=session_id, includeSelf=True)