import { createContext, useContext, useEffect, useState } from "react"
import { useLocalStorage } from "@uidotdev/usehooks";

const PreferencesContext = createContext({
volume: 100,
allowVoice: true,
useCustomCursor: true,
setVolume: (volume) => {},
setAllowVoice: (allowVoice) => {},
setUseCustomCursor: (useCustomCursor) => {}
})

export default function PreferencesProvider({ children }) {

    const [volume, setVolume] = useLocalStorage("volume", 100)
    const [allowVoice, setAllowVoice] = useLocalStorage("allowVoice", true)
    const [useCustomCursor, setUseCustomCursor] = useLocalStorage("useCustomCursor", true)

    // useEffect(() => {
    //     document.body.style.cursor = "cursor:url('/static/cursor.ico'), pointer;"
    // }, [useCustomCursor])

  return <PreferencesContext.Provider value={{ volume, allowVoice, useCustomCursor, setVolume, setAllowVoice, setUseCustomCursor,  }}>{children}</PreferencesContext.Provider>
}

export function usePreferences() {
  return useContext(PreferencesContext)
}
