import { createContext, useContext, useState } from 'react';

const UserContext = createContext({
  user: null,
  setUser: (user) => {},
});

export default function UserProvider({ children }) {
  const [user, setUser] = useState(/** @type {User} */null);

  return <UserContext.Provider value={{ user, setUser }}>{children}</UserContext.Provider>
}

export function useUser() {
  return useContext(UserContext);
}