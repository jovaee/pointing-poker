import { useNavigate } from "react-router-dom"

import axios from "axios"

import { useState } from "react"

import { useDisclosure, VStack, Button, Box, Heading, CheckboxGroup, Checkbox } from "@chakra-ui/react"

import BasicModal from "@/components/BasicModal"
import Section from "@/components/Section"

// TODO: Get from API
const POINTS = [["?", 0.5, 1, 2, 3, 5, 8, 13, 21, 34, 55, 100]]

const SessionSetup = () => {
  const navigate = useNavigate()

  const { isOpen, onOpen, onClose } = useDisclosure()

  const [selectedPoints, setSelectedPoints] = useState(0)
  const [modalContent, setModalContent] = useState({
    headerText: "",
    bodyText: "",
  })

  const createSession = () => {
    axios
      .post("/api/session/")
      .then((res) => {
        // Navigate the user to the user create page
        navigate(`/session/${res.data.session_id}/user`)
      })
      .catch(() => {
        setModalContent({
          headerText: "An Error Occurred",
          bodyText: "Failed to create new session",
        })
        onOpen()
      })
  }

  return (
    <>
      <BasicModal
        isOpen={isOpen}
        onClose={onClose}
        headerText={modalContent.headerText}
        bodyText={modalContent.bodyText}
      />

      <VStack alignItems={"left"} spacing={"2rem"}>
        <Section>
          <Heading size={"lg"} paddingBottom={"0.5rem"} textAlign={"center"}>
            Settings
          </Heading>
          <Box>
            <Heading size={"sm"} paddingBottom={"0.5rem"}>
              Points
            </Heading>
            <VStack alignItems={"left"}>
              <CheckboxGroup>
                {POINTS.map((points, index) => {
                  return <Checkbox key={`point${index}`} isChecked={index === selectedPoints}>{points.join(" / ")}</Checkbox>
                })}
              </CheckboxGroup>
            </VStack>
          </Box>
        </Section>

        <Button colorScheme={"blue"} onClick={createSession}>
          Create
        </Button>
        <Button colorScheme={"gray"} onClick={() => navigate("/")}>
          Back
        </Button>
      </VStack>
    </>
  )
}

export default SessionSetup
