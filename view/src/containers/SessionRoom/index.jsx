import React, { useEffect } from "react"
import { Heading, VStack, Stack, Input, Button, Wrap } from "@chakra-ui/react"
import { CheckIcon } from "@chakra-ui/icons"

import Section from "@/components/Section"

import { useParams, useNavigate } from "react-router-dom"

import PointCard from "@/components/PointCard"
import useRoomSocket from "@/hooks/Room"
import { useUser } from "@/contexts/User"
import StatsGrid from "@/components/StatsGrid"
import { usePreferences } from "@/contexts/Preferences"

// TODO: Make configurable from settings and propagate to other users
const SCORES = ["?", 0.5, 1, 2, 3, 5, 8, 13, 21, 34, 55, 100]

// TODO: Show a green border for a second when another user changes their score
const SessionRoom = () => {
  const navigate = useNavigate()
  const params = useParams()

  const user = useUser()
  const preferences = usePreferences()
  const room = useRoomSocket(params.sessionId, user.user?.user_id)

  useEffect(() => {
    if (!user.user) {
      navigate(`/session/${params.sessionId}/user`, { replace: true })
      return
    }

    room.connect()
  }, [])

  return (
    <Stack spacing="1rem">
      <Section>
        <Heading size="lg" paddingBottom="0.5rem" textAlign="center">
          Description
        </Heading>
        <VStack alignItems="">
          <Input
            placeholder="Enter a description"
            value={room.session?.description}
            isDisabled={user.user?.is_spectator}
            onChange={(e) => onDescriptionChange(e.target.value)}
          />
          {!user.user?.is_spectator && (
            <>
              <Button onClick={room.reset}>Reset</Button>
              <Button onClick={room.flip}>
                Flip Cards
                <CheckIcon
                  height={4}
                  width={4}
                  textAlign="right"
                  color="green.400"
                  position="absolute"
                  right={10}
                  visibility={room.isFlipped ? "visible" : "hidden"}
                />
              </Button>
            </>
          )}
        </VStack>
      </Section>

      {/* Point buttons */}
      {!user.user?.is_spectator && (
        <Section>
          <Heading size="lg" paddingBottom="0.5rem" textAlign="center">
            Points
          </Heading>
          <Wrap justify="center" spacing={3}>
            {SCORES.map((score) => {
              return (
                <Button
                  key={`point${score}`}
                  colorScheme="blue"
                  size="sm"
                  width="60px"
                  onClick={() => room.vote(score)}
                >
                  {score}
                </Button>
              )
            })}
          </Wrap>
        </Section>
      )}

      {/* Stats */}
      <Section>
        <Heading size="lg" paddingBottom="0.5rem" textAlign="center">
          Statistics
        </Heading>

        <StatsGrid
          isFlipped={room.isFlipped || user.user?.is_spectator}
          stats={room.stats}
          speak={preferences.allowVoice && room.speak}
        />
      </Section>

      {/* Point cards */}
      <Section>
        <Heading size="lg" paddingBottom="0.5rem" textAlign="center">
          Results
        </Heading>
        <Wrap justify="center" spacing={3}>
          {room.users
            ?.filter((item) => !item.is_spectator)
            .map((item) => {
              return (
                <PointCard
                  key={`pc${item.user_id}`}
                  name={item.username}
                  score={item.score}
                  oldScore={item.old_score}
                  flipped={room.isFlipped || user.user.is_spectator}
                  voteFlagged={item.vote_flagged}
                  backgroundImageUrl={item.background_image_url}
                  selfCard={item.user_id === user.user.user_id}
                />
              )
            })}
        </Wrap>
      </Section>
    </Stack>
  )
}

export default SessionRoom
