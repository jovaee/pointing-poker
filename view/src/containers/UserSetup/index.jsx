import { useNavigate, useParams } from "react-router-dom"

import axios from "axios"

import { useEffect, useState } from "react"

import { useDisclosure, Checkbox, VStack, Button, Input, Heading, HStack, IconButton, Tooltip } from "@chakra-ui/react"
import { RepeatIcon, QuestionOutlineIcon } from "@chakra-ui/icons"

import BasicModal from "@/components/BasicModal"
import RandomGifModal from "@/components/RandomGifModal"
import Section from "@/components/Section"
import { useUser } from "@/contexts/User"

const UserSetup = () => {
  const navigate = useNavigate()
  const params = useParams()

  const user = useUser()

  const { isOpen, onOpen, onClose } = useDisclosure()
  const { isOpen: isGifGenerateOpen, onOpen: onGifGenerateOpen, onClose: onGifGenerateClose } = useDisclosure()

  const [sessionExists, setSessionExists] = useState(false)
  const [username, setUsername] = useState("")
  const [isSpectator, setIsSpectator] = useState(false)
  const [backgroundImageUrl, setBackgroundImageUrl] = useState("")
  const [modalContent, setModalContent] = useState({
    headerText: "",
    bodyText: "",
  })

  useEffect(() => {
    // Check that the session exists
    axios
      .get(`/api/session/${params.sessionId}`)
      .then(() => {
        setSessionExists(true)

        const userData = JSON.parse(localStorage.getItem("user") ?? "{}")
        setUsername(userData.username ?? "")
        setIsSpectator(userData.isSpectator?? false)
        setBackgroundImageUrl(userData.backgroundImageUrl ?? "")
      })
      .catch((err) => {
        let message
        switch (err.response?.status) {
          case 404:
            message = `Session with code "${params.sessionId}" doesn't exist`
            break
          default:
            message = "Failed to join session due to error"
        }

        setModalContent({ headerText: "An Error Occurred", bodyText: message })
        onOpen()
      })
  }, [])

  const createUserAndJoin = () => {
    const userData = { username, backgroundImageUrl, session_id: params.sessionId, is_spectator: isSpectator }

    axios
      .post("/api/user", userData)
      .then((res) => {
        localStorage.setItem("user", JSON.stringify({username, backgroundImageUrl, isSpectator}))

        user.setUser(res.data)
        navigate(`/session/${params.sessionId}/room`)
      })
      .catch((err) => {
        setModalContent({ headerText: "An Error Occurred", bodyText: err.message })
        onOpen()
      })
  }

  const generateRandomName = () => {
    axios
      .get("/api/generate/name")
      .then((res) => {
        setUsername(res.data.name)
      })
      .catch(() => {
        setModalContent({
          headerText: "An Error Occurred",
          bodyText: "Failed to generate random name",
        })
        onOpen()
      })
  }

  const acceptRandomGif = (url) => {
    setBackgroundImageUrl(url)
    onGifGenerateClose()
  }

  return (
    <>
      <BasicModal
        isOpen={isOpen}
        onClose={onClose}
        headerText={modalContent.headerText}
        bodyText={modalContent.bodyText}
      />

      <RandomGifModal isOpen={isGifGenerateOpen} onClose={onGifGenerateClose} accept={acceptRandomGif} />

      <VStack alignItems="left" spacing="2rem">
        <Section>
          <Heading size="lg" paddingBottom="0.5rem" textAlign="center">
            User
          </Heading>
          <HStack>
            <Input placeholder="Enter username" value={username} onChange={(e) => setUsername(e.target.value)} />
            <Tooltip label="Generate random name" aria-label="A tooltip">
              <IconButton onClick={generateRandomName} tooltip="Generate random name" icon={<RepeatIcon />} />
            </Tooltip>
          </HStack>
          <HStack marginTop={3}>
            <Input
              placeholder="(Optional) Enter a link to an Image/GIF"
              value={backgroundImageUrl}
              onChange={(e) => setBackgroundImageUrl(e.target.value)}
            />
            <Tooltip label="Generate random GIF" aria-label="A tooltip">
              <IconButton onClick={onGifGenerateOpen} tooltip="Generate random GIF" icon={<RepeatIcon />} />
            </Tooltip>
          </HStack>
        </Section>

        <Section>
          <Heading size="lg" paddingBottom="0.5rem" textAlign="center">
            Additional Settings
          </Heading>
          <HStack spacing={1}>
            <Checkbox isChecked={isSpectator} onChange={(event) => setIsSpectator(event.target.checked)}>Spectator</Checkbox>
            <Tooltip label="Join as a spectator. Other users' scores will always be visible but you won't be able to vote.">
              <QuestionOutlineIcon />
            </Tooltip>
          </HStack>
        </Section>

        <Button colorScheme="blue" onClick={createUserAndJoin} isDisabled={!username.length || !sessionExists}>
          Join
        </Button>
        <Button colorScheme="gray" onClick={() => navigate("/")}>
          Back
        </Button>
      </VStack>
    </>
  )
}

export default UserSetup
