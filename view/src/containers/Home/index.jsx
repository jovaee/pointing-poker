import React, { useState } from "react"
import axios from "axios"
import { useNavigate } from "react-router-dom"
import { Stack, Button, Input, Heading, VStack, useDisclosure } from "@chakra-ui/react"

import BasicModal from "@/components/BasicModal"

const Home = () => {
  const navigate = useNavigate()

  const { isOpen, onOpen, onClose } = useDisclosure()
  const [sessionId, setSessionId] = useState("")
  const [modalContent, setModalContent] = useState({
    headerText: "",
    bodyText: "",
  })

  const createSession = () => {
    navigate("/session/setup")
  }

  const joinSession = () => {
    axios
      .get(`/api/session/${sessionId}`)
      .then((res) => navigate(`/session/${res.data.session_id}/user`, { session: res.data }))
      .catch((err) => {
        let message
        switch (err.response?.status) {
          case 404:
            message = `Session with code "${sessionId || "<blank>"}" doesn't exist`
            break
          default:
            message = "Failed to join session due to error"
        }

        setModalContent({ headerText: "An Error Occurred", bodyText: message })
        onOpen()
      })
  }

  return (
    <Stack spacing="2rem">
      <BasicModal
        isOpen={isOpen}
        onClose={onClose}
        headerText={modalContent.headerText}
        bodyText={modalContent.bodyText}
      />

      <VStack alignItems="left">
        <Heading size="md">New Session</Heading>
        <Button colorScheme={"blue"} onClick={createSession}>
          Create
        </Button>
      </VStack>

      <VStack align="left">
        <Heading size="md">Join existing</Heading>
        <Input placeholder="Enter session code" value={sessionId} onChange={(e) => setSessionId(e.target.value)} />
        <Button colorScheme="blue" onClick={joinSession}>
          Join
        </Button>
      </VStack>
    </Stack>
  )
}

export default Home
