import React, { useState } from "react"
import { Container } from "@chakra-ui/react"
import { BrowserRouter, Routes, Route } from "react-router-dom"

import Home from "@/containers/Home"
import PointingNavbar from "@/components/Navbar"
import SessionRoom from "@/containers/SessionRoom"
import SessionSetup from "@/containers/SessionSetup"
import UserSetup from "@/containers/UserSetup"
import UserProvider from "@/contexts/User"
import SettingsDrawer from "./components/SettingsDrawer"
import PreferencesProvider from "./contexts/Preferences"

const App = () => {
  const [isSettingsDrawerOpen, setSettingsDrawerOpen] = useState(false)

  return (
    <BrowserRouter>
      <PointingNavbar toggleSettingsDrawer={() => setSettingsDrawerOpen((v) => !v)} />

      <Container maxWidth="6xl">
        <PreferencesProvider>
          <UserProvider>
            <SettingsDrawer isOpen={isSettingsDrawerOpen} onClose={() => setSettingsDrawerOpen(false)}/>
            <Routes>
              <Route path="/session/setup" element={<SessionSetup />} />
              <Route path="/session/:sessionId/user" element={<UserSetup />} />
              <Route path="/session/:sessionId/room" element={<SessionRoom />} />
              <Route exact path="/" element={<Home />} />
              <Route path="*" element={<NotFound />} />
            </Routes>
          </UserProvider>
        </PreferencesProvider>
      </Container>
    </BrowserRouter>
  )
}

const NotFound = () => {
  return <div>Not Found</div>
}

export default App
