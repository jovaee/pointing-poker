export const toastDefaults = (options) => {
    return {
      ...{
        title: 'Yo',
        status: 'info',
        duration: 2500,
        variant: 'solid',
        position: 'top-right',
        isClosable: true,
      },
      ...options,
    };
  };