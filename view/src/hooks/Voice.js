import { usePreferences } from "@/contexts/Preferences"
import { useState, useEffect } from "react"

const useVoice = () => {
  const preferences = usePreferences()

  const [utterance, setUtterance] = useState()
  const [isSpeaking, setIsSpeaking] = useState(false)

  useEffect(() => {
    const instance = new SpeechSynthesisUtterance()

    // TODO: Don't think this these event listeners are working :(
    instance.addEventListener("start", () => setIsSpeaking(true))
    instance.addEventListener("end", () => setIsSpeaking(false))
    instance.addEventListener("error", () => setIsSpeaking(false))

    setUtterance(instance)
  }, [])

  useEffect(() => {
    if (utterance) {
      utterance.volume = preferences.volume / 100.0
    }
  }, [utterance, preferences.volume])

  /**
   *
   * @param {string} phrase
   */
  const say = (phrase) => {
    if (utterance && !isSpeaking && phrase) {
      utterance.text = phrase
      window.speechSynthesis.speak(utterance)
    }
  }

  return {
    say,
  }
}

export default useVoice
