const useMousePos = () => {
  const [mousePos, setMousePos] = useState({ posX: 0, posY: 0 })
  useEffect(() => {
    const getMousePos = debounce((e) => {
      setMousePos({ posX: e.clientX, posY: e.clientY })
    }, 25)

    document.addEventListener("mousemove", getMousePos)

    return () => {
      document.removeEventListener("mousemove", getMousePos)
    }
  })

  return mousePos
}
