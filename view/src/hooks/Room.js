import { useState, useEffect } from "react"

import { io } from "socket.io-client"

import { useToast } from "@chakra-ui/react"
import { toastDefaults } from "@/lib/consts"
import useVoice from "@/hooks/Voice"
import { usePreferences } from "@/contexts/Preferences"

const useRoomSocket = (sessionId, userId) => {
  const [socket, setSocket] = useState()

  const [users, setUsers] = useState([])
  const [session, setSession] = useState()
  const [stats, setStats] = useState({})

  const [isDisconnecting, setIsDisconnecting] = useState(false)

  const voice = useVoice()
  const toast = useToast()
  const preferences = usePreferences()

  useEffect(() => {
    if (socket) {
      listen()
    }

    return () => {
      setIsDisconnecting(true)
      socket?.disconnect()
    }
  }, [socket])

  useEffect(() => {
    // TODO: NOT WORKING
    if (preferences.allowVoice) {
      socket?.on("say", onSay)
    } else {
      socket?.off("say", onSay)
    }
  }, [socket, preferences.allowVoice])

  /**
   *
   */
  const connect = () => {
    const _socket = io()

    setSocket(_socket)
  }

  const isConnected = socket?.connected === true
  const isFlipped = session?.flipped === 1

  const onSay = (data) => {
    voice.say(data.phrase)
  }

  /**
   *
   */
  const listen = () => {
    socket?.on("connect", () => {
      socket.emit("join", { session_id: sessionId, user_id: userId })
    })
    socket?.on("disconnect", (reason) => {
      // If the disconnection is expected then don't do anything
      if (isDisconnecting) return

      toast(
        toastDefaults({
          title: "Session connection lost",
          description: "Oh no... You were disconnected. Attempting to reconnect to the servers",
          status: "error",
        })
      )
    })
    socket?.on("usersChanged", (data) => setUsers(data.users))
    socket?.on("sessionUpdated", (data) => setSession(data.session))
    socket?.on("sessionStatsUpdated", (data) => setStats(data))
    socket?.on("notification", (data) =>
      toast(toastDefaults({ title: data.title, description: data.description, status: data.status }))
    )
    // socket?.on("say", onSay)
  }

  /**
   *
   * @param score
   */
  const vote = (score) => {
    socket?.emit("vote", { score })
  }

  /**
   *
   */
  const reset = () => {
    socket?.emit("reset")
  }

  /**
   *
   */
  const flip = () => {
    socket?.emit("flip")
  }

  /**
   *
   * @param description
   */
  const descriptionChange = (description) => {}

  /**
   *
   * @param name
   * @param value
   */
  const speak = (name, value) => {
    if (!value) return
    socket?.emit("speak", { name, value })
  }

  return {
    isConnected,
    isFlipped,

    users,
    stats,
    session,

    connect,
    vote,
    reset,
    flip,
    descriptionChange,
    speak,
  }
}

export default useRoomSocket
