import React, { useEffect, useState } from "react"
import {
  useColorMode,
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerCloseButton,
  DrawerFooter,
  DrawerHeader,
  FormLabel,
  Switch,
  HStack,
  Slider,
  SliderTrack,
  SliderThumb,
  SliderFilledTrack,
  VStack,
  Box,
  Text,
  SliderMark,
  Tooltip,
} from "@chakra-ui/react"
import { MoonIcon, SunIcon, InfoIcon } from "@chakra-ui/icons"
import { FaVolumeOff, FaVolumeLow, FaVolumeHigh } from "react-icons/fa6"
import { usePreferences } from "@/contexts/Preferences"

const SettingsDrawer = ({ isOpen, onClose }) => {
  const preferences = usePreferences()
  const { colorMode, toggleColorMode } = useColorMode()

  const [currentVolume, setCurrentVolume] = useState(preferences.volume)

  const getVolumeIcon = () => {
    if (currentVolume === 0) {
      return <FaVolumeOff size={20} />
    } else if (currentVolume > 0 && currentVolume <= 50) {
      return <FaVolumeLow size={20} />
    } else {
      return <FaVolumeHigh size={20} />
    }
  }

  return (
    <Drawer isOpen={isOpen} placement="right" onClose={onClose}>
      <DrawerContent>
        <DrawerCloseButton />
        <DrawerHeader>Settings</DrawerHeader>

        <DrawerBody>
          <VStack width="100%" align="left" gap={10}>
            <Box>
              <FormLabel htmlFor="color-mode">Custom cursor</FormLabel>
              <HStack gap={4}>
                <Text>No</Text>
                <Switch
                  id="color-mode"
                  disabled
                  isChecked={preferences.useCustomCursor}
                  onChange={() => preferences.setUseCustomCursor(!preferences.useCustomCursor)}
                />
                <Text>Yes</Text>
              </HStack>
            </Box>

            <Box>
              <FormLabel htmlFor="color-mode">Colour mode</FormLabel>
              <HStack gap={4}>
                <SunIcon />
                <Switch id="color-mode" isChecked={colorMode === "dark"} onChange={toggleColorMode} />
                <MoonIcon />
              </HStack>
            </Box>

            <Box>
              <FormLabel htmlFor="color-mode">
                Points Voice
                <Tooltip label="Refresh page to take effect">
                  <InfoIcon marginLeft={2} />
                </Tooltip>
              </FormLabel>
              <HStack gap={4}>
                <Text>No</Text>
                <Switch
                  id="color-mode"
                  isChecked={preferences.allowVoice}
                  onChange={() => preferences.setAllowVoice(!preferences.allowVoice)}
                />
                <Text>Yes</Text>
              </HStack>
            </Box>

            <Box>
              <FormLabel htmlFor="volume">
                Volume
                <Tooltip label="Not all browsers supported">
                  <InfoIcon marginLeft={2} />
                </Tooltip>
              </FormLabel>
              <HStack>
                {getVolumeIcon()}
                <Slider
                  id="volume"
                  defaultValue={preferences.volume}
                  onChange={setCurrentVolume}
                  onChangeEnd={preferences.setVolume}
                  disabled={!preferences.allowVoice}
                >
                  <SliderMark value={currentVolume} textAlign="center" mt="3" ml="-5" w="12">
                    {currentVolume}%
                  </SliderMark>
                  <SliderTrack>
                    <SliderFilledTrack />
                  </SliderTrack>
                  <SliderThumb />
                </Slider>
              </HStack>
            </Box>
          </VStack>
        </DrawerBody>

        {/* <DrawerFooter>
            <Button colorScheme='blue'>Save</Button>
          </DrawerFooter> */}
      </DrawerContent>
    </Drawer>
  )
}

export default SettingsDrawer
