import { Box, Heading, VStack, useColorModeValue } from "@chakra-ui/react"
import { CheckIcon, ArrowForwardIcon } from "@chakra-ui/icons"
import { isNull } from "lodash"

import "./index.css"

// TODO: When the score is rendered the 'name' header get moved upwards since the score header doesn't occupy any space
// before that.Figure out how to not make it move around

const PointCard = ({ name, score, oldScore, flipped, voteFlagged, backgroundImageUrl, selfCard }) => {
  const borderColor = selfCard ? "red.300" : score !== null ? "blue.300" : null
  const lightBackground = voteFlagged ? (selfCard ? "red.100" : "blue.100") : "gray.200"
  const darkBackground = voteFlagged ? (selfCard ? "red.200" : "blue.200") : "gray.600"

  return (
    <Box className="flip-card" width={200} height={250}>
      <Box className={`flip-card-inner ${flipped ? "" : "flipped"}`}>
        {/* When the card is flipped */}
        <Box
          className="flip-card-front"
          background={useColorModeValue(lightBackground, darkBackground)}
          border="1px"
          borderRadius={10}
          borderColor={borderColor}
          borderWidth={3}
        >
          <VStack height="full" justifyContent="space-around" alignContent="center">
            <Heading size="md" textAlign="center" overflowWrap="anywhere">
              {name}
            </Heading>
            <Heading
              className="score"
              size="lg"
              textAlign="center"
              visibility={!flipped && !selfCard ? "hidden" : "visible"}
            >
              {voteFlagged ? (
                <Box display="flex" alignItems="center">
                  {!isNull(oldScore) ? oldScore : "_"}
                  <ArrowForwardIcon height={4} width={6} />
                  {score}
                </Box>
              ) : (
                score
              )}
            </Heading>
            {score !== null ? (
              <CheckIcon height={8} width={8} textAlign={"center"} color={"red.400"} />
            ) : (
              <Box height={8} width={8} />
            )}
          </VStack>
        </Box>
        {/* When the card is not flipped */}
        <Box
          className="flip-card-back"
          background={useColorModeValue(lightBackground, darkBackground)}
          backgroundImage={backgroundImageUrl}
          backgroundPosition="center"
          backgroundSize="cover"
          backgroundRepeat="no-repeat"
          border={"1px"}
          borderRadius={10}
          borderColor={borderColor}
          borderWidth={3}
        >
          <VStack height="full" justifyContent="space-around" alignContent="center">
            <Heading size="md" textAlign="center" overflowWrap="anywhere">
              {name}
            </Heading>
            <Heading className="score" size="lg" textAlign="center" visibility={!selfCard ? "hidden" : "visible"}>
              {score}
            </Heading>
            {score !== null ? (
              <CheckIcon height={8} width={8} textAlign="center" color="red.400" />
            ) : (
              <Box height={8} width={8} />
            )}
          </VStack>
        </Box>
      </Box>
    </Box>
  )
}

export default PointCard
