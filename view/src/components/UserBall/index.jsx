import { Box, useColorModeValue } from "@chakra-ui/react"

const UserBall = ({ posX, posY, name }) => {
  return (
    <Box
      borderColor={useColorModeValue("black", "gray.100")}
      borderWidth={0.5}
      borderRadius={50}
      background={useColorModeValue("yellow.200", "yellow.200")}
      color={"black"}
      height={30}
      width={30}
      position="fixed"
      zIndex={100}
      textAlign="center"
      left={`${posX}px`}
      top={`${posY}px`}
    >
      {name}
    </Box>
  )
}

export default UserBall
