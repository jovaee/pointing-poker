import { Box, useColorModeValue } from "@chakra-ui/react"

const Section = ({ children }) => {
  return (
    <Box
      padding={"1.2em 1.2rem 1.2rem 1.2rem"}
      borderRadius={10}
      background={useColorModeValue("gray.50", "whiteAlpha.100")}
    >
      {children}
    </Box>
  )
}

export default Section
