import { useState } from "react"

import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Image,
  HStack,
  Spinner,
  Flex,
} from "@chakra-ui/react"

import axios from "axios"

const RandomGifModal = ({ isOpen, onClose, accept }) => {
  const [searchPhrase, setSearchPhrase] = useState("")
  const [gifLink, setGifLink] = useState("")
  const [isLoading, setLoading] = useState(false)

  const searchGif = () => {
    setLoading(true)
    axios
      .get(`/api/generate/gif?phrase=${searchPhrase}`)
      .then((res) => {
        setGifLink(res.data.url)
        setLoading(false)
      })
      .catch((err) => {
        alert(err)
        setLoading(false)
      })
  }

  return (
    <Modal isOpen={isOpen} onClose={onClose} size={"lg"} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Search GIF by phrase</ModalHeader>
        <ModalBody>
          <HStack>
            <Input
              placeholder="Enter search phrase"
              value={searchPhrase}
              onChange={(e) => setSearchPhrase(e.target.value)}
            />
            <Button colorScheme="blue" mr={3} onClick={searchGif} isDisabled={!searchPhrase}>
              Search
            </Button>
          </HStack>
          <Flex justify="center" marginTop={3}>
            {isLoading ? <Spinner /> : null}
            {!isLoading ? <Image marginTop={3} src={gifLink} loading="eager" /> : null}
          </Flex>
        </ModalBody>
        <ModalFooter>
          <Button colorScheme="gray" mr={3} onClick={onClose}>
            Cancel
          </Button>
          <Button colorScheme="blue" mr={3} onClick={() => accept(gifLink)}>
            Use
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default RandomGifModal
