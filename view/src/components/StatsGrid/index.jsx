import { Grid, GridItem, IconButton, Text } from "@chakra-ui/react"
import { FaVolumeUp } from "react-icons/fa"


const SpeakOptions = Object.freeze({
    Average: "Average", ClosestFib: "ClosestFib", MostCommon: "MostCommon", Maximum: "Maximum", Minimum: "Minimum",
})


export default function StatsGrid({ isFlipped, stats, speak }) {
  const talkButton = (name, value) => {
    return (
      <IconButton
        variant="unstyled"
        aria-label="Call Sage"
        size="xs"
        icon={<FaVolumeUp />}
        hidden={!speak}
        isDisabled={!speak || !isFlipped}
        onClick={() => speak(name, value)}
      />
    )
  }

  return (
    <Grid templateColumns="repeat(2, 1fr)">
      <Grid templateColumns="repeat(2, 1fr)">
        <GridItem colSpan={1}>{talkButton(SpeakOptions.Average, stats.average)}Average:</GridItem>
        <GridItem colSpan={1}>
          <Text color="green">{isFlipped && stats.average}</Text>
        </GridItem>

        <GridItem colSpan={1}>{talkButton(SpeakOptions.ClosestFib, stats.closest_fib)}Closest Fibonacci:</GridItem>
        <GridItem colSpan={1}>{isFlipped && stats.closest_fib}</GridItem>

        <GridItem colSpan={1}>{talkButton(SpeakOptions.MostCommon, stats.most_common)}Most Common:</GridItem>
        <GridItem colSpan={1}>{isFlipped && stats.most_common}</GridItem>
      </Grid>

      <Grid templateColumns="repeat(2, 1fr)">
        <GridItem colSpan={1}>{talkButton(SpeakOptions.Maximum, stats.max)}Maximum:</GridItem>
        <GridItem colSpan={1}>{isFlipped && stats.max}</GridItem>

        <GridItem colSpan={1}>{talkButton(SpeakOptions.Minimum, stats.min)}Minimum:</GridItem>
        <GridItem colSpan={1}>{isFlipped && stats.min}</GridItem>

        <GridItem colSpan={1}>&nbsp;</GridItem>
      </Grid>
    </Grid>
  )
}
