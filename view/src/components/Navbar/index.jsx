import React from "react"
import { Image, Box, HStack, IconButton, Flex, Heading } from "@chakra-ui/react"
import { SettingsIcon } from "@chakra-ui/icons"

import { useNavigate } from "react-router-dom"

import Logo from "./logo.png"

const PointingNavbar = ({ toggleSettingsDrawer }) => {
  const navigate = useNavigate()

  return (
    <Box
      padding="0.75rem 0 0.5rem 1rem"
      marginBottom="3rem"
      borderBottom="2px"
    >
      <Flex justifyContent="space-between">
        <HStack alignItems="center">
          <Heading
            cursor="default"
            onClick={() => navigate("/", { replace: true })}
          >
            Pointing Kakegurui
          </Heading>
          <Image
            boxSize="40px"
            objectFit="scale-down"
            src={Logo}
          />
        </HStack>
        <IconButton
          marginRight="1rem"
          onClick={toggleSettingsDrawer}
          variant="unstyled"
          icon={<SettingsIcon />}
        />
      </Flex>
    </Box>
  )
}

export default PointingNavbar
