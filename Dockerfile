# Build frontend
FROM node:20-alpine

WORKDIR /app

COPY view/package.json .
COPY view/package-lock.json .

RUN npm install

COPY view/ .

RUN npm run build

# API
FROM python:3.12-slim

WORKDIR /pointing-kakegurui

ENV APPDATA=/pointing-kakegurui/appdata

COPY --from=0 /app/dist view/dist/

COPY . .

RUN pip install -U pip && pip install -r requirements.txt

CMD ./run.sh
