#!/bin/bash
export PORT=443

gunicorn --certfile=certs/server.crt --keyfile=certs/server.key --worker-class eventlet -w 1 server:app
